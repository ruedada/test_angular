import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { DetailsComponent } from './details/details.component';
import { BusquedaComponent } from './busqueda/busqueda.component';

const routes: Routes = [
  {
    path: '',
    component: PostsComponent,
    data: { title: 'Posts List' }
  },
  {
    path: 'posts',
    component: PostsComponent,
    data: { title: 'Posts List' }
  },
  {
    path: 'details/:id',
    component: DetailsComponent,
    data: { title: 'Post Details' }
  },
  {
    path: 'busqueda/:palabra',
    component: BusquedaComponent,
    data: { title: 'Busqueda' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
