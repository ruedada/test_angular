import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from '../services.service';
import { Post } from '../interfaces';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  postId;
  post: Post[] = [{
    userId: 0,
    id: 0,
    title: '',
    body: ''
  }];

  constructor(private route: ActivatedRoute, private api: ServicesService, private router: Router) {
    this.route.queryParams.subscribe(
      params => {
        this.postId =  params['id'];
      }
    );

    this.api.getPostsId(this.postId).subscribe(resId => {
      this.post = resId;
      // console.log(this.post);
    }, err => {
      // console.log(err);
    });
  }

  ngOnInit() {
  }

}
