import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { Post } from '../interfaces';
import {Router, NavigationExtras, ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  data: Post[] = [];

  public searchForm = new FormGroup({
    searchText: new FormControl(''),
  });

  constructor( private api: ServicesService, private router: Router, private route: ActivatedRoute ) {
   }

  ngOnInit() {
    this.getPosts();
  }

  search(search) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        'search': search,
      },
    };
    this.router.navigate(['busqueda/' + search], navigationExtras);
  }

  getPosts() {
    this.api.getPosts().subscribe(res => {
      this.data = res;
    }, err => {
      console.log(err);
    });
  }

  details(postId) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        'id': postId,
      },
    };
    this.router.navigate(['details/' + postId], navigationExtras);
  }

}
