import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'busqueda'
})
export class BusquedaPipe implements PipeTransform {

  transform(data: any[], resBusqueda?: any): any {
    if (!data) {
      // console.log('no data');
      return [];
    }

    if (!resBusqueda) {
      // console.log('data');
      return data;
    }

    resBusqueda = resBusqueda.toLowerCase();

    return data.filter( it => {
      const id = it.id.toString().includes(resBusqueda);
      const title = it.title.toLowerCase().includes(resBusqueda.toLowerCase());
      const body = it.body.toLowerCase().includes(resBusqueda.toLowerCase());
      return (id + title + body);
    });
  }

}
