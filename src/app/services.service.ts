import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Post } from './interfaces';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "https://jsonplaceholder.typicode.com/posts";

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor( private http: HttpClient ) { }

  getPosts (): Observable<Post[]> {
    return this.http.get<Post[]>(apiUrl)
      .pipe(
        tap(posts => console.log('fetched data'))
      );
  }
  getPostsId (id): Observable<Post[]> {
    const idApi = `${apiUrl}/${id}`;
    return this.http.get<Post[]>(idApi)
      .pipe(
        tap(posts => console.log('fetched data w/id'))
      );
  }

}
