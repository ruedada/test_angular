import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ServicesService } from '../services.service';
import { Post } from '../interfaces';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  data: Post[] = [];
  resBusqueda;

  constructor(private route: ActivatedRoute, private api: ServicesService, private router: Router) { 
    this.route.queryParams.subscribe(
      params => {
        this.resBusqueda =  params['search'];
      }
    );
  }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    this.api.getPosts().subscribe(res => {
      this.data = res;
    }, err => {
    });
  }

  details(postId) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        'id': postId,
      },
    };
    this.router.navigate(['details/' + postId], navigationExtras);
  }

}
